<?php
include("database.php");
$departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
$stmt = $conn->prepare("SELECT * FROM students WHERE students.department LIKE '%".$_POST['department']."%' AND students.name LIKE '%".$_POST['keyword']."%'");
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$result = $stmt->fetchAll();
$html = "";
foreach($result as $key => $value) {
    $html .= " <tr class='mb-3'>
    <td>".
        $value['id']."
    </td>
    <td>".
        $value['name']."
    </td>
    <td>".
        $departments[$value['department']]."
    </td>
    <td class='d-flex justify-content-center'><button class='me-2 db-btn'>Xóa</button>
    <button class='db-btn'>Sửa</button></td>
</tr>";
}
echo json_encode(
    array("status"=>"Success",
    "data" => $html)
);
?>