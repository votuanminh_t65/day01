<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title>Đăng ký Tân sinh viên</title>
</head>

<body>
    <form action="" class="bd-blue">
        <table class="w-100">
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="name">Họ và tên</label></td>
                <td class="bd-blue">
                    <input type="text" id="name" name="name" required class=" w-100 p-10-20 h-100">
                </td>
            </tr>
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="gender">Giới tính</label>
                </td>
                <td>
                    <div class="d-flex">
                        <?php
                        $values = array(0 => 'Nam', 1 => 'Nữ');
                        $genders = array(0 => 'men', 1 => 'women');
                        for ($i = 0; $i < count($genders); $i++) {
                            $key = $genders[$i];
                            $value = $values[$i];

                            echo "<div class='genders'>
                            <input id='$key' type='radio' name='genders' value='$value'>
                            <label for='$key'>$value</label>
                        </div>";
                        }

                        ?>
                    </div>

                </td>

            </tr>
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="department">Phân
                        khoa</label></td>
                <td>

                    <select id="department" name="department" class="bd-blue py-10">
                        <option value="">--Chọn phân khoa--</option>
                        <?php
                        $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                        foreach ($departments as $key => $value) {
                            echo "<option value=\"$key\">$value</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
        </table>
        <div class="text-center ">
            <button type="submit" class="btn bd-blue bg-green text-white mt-20">Đăng ký</button>
        </div>
    </form>
</body>

</html>