<?php
include "database.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $id = $_POST['id'];
    $name = $_POST['name'];
    $gender = $_POST['gender'];
    $department = $_POST['department'];
    $birthdate = $_POST['birthdate'];
    $address = $_POST['address'];
    // Add other form fields as needed

    // Update the student information in the database
    $stmt = $conn->prepare("UPDATE students SET name = :name, gender = :gender, department = :department, birthdate = :birthdate, address = :address WHERE id = :id");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':gender', $gender);
    $stmt->bindParam(':department', $department);
    $stmt->bindParam(':birthdate', $birthdate);
    $stmt->bindParam(':address', $address);
    $stmt->bindParam(':id', $id);

    if ($stmt->execute()) {
        // Redirect to index.php after successful update
        header("Location: index.php");
        exit();
    } else {
        echo "Error updating student information.";
    }
} else {
    echo "Invalid request.";
}
?>