<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Validation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        function updateDistricts() {
            var selectedCity = document.getElementById("thanh_pho").value;
            var quanSelect = document.getElementById("quan");
            quanSelect.innerHTML = ""; // Xóa các quận hiện tại

            if (selectedCity === "Hà Nội") {
                var hanoiDistricts = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
                for (var i = 0; i < hanoiDistricts.length; i++) {
                    var option = document.createElement("option");
                    option.text = hanoiDistricts[i];
                    quanSelect.add(option);
                }
            } else if (selectedCity === "TP.Hồ Chí Minh") {
                var hcmDistricts = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
                for (var i = 0; i < hcmDistricts.length; i++) {
                    var option = document.createElement("option");
                    option.text = hcmDistricts[i];
                    quanSelect.add(option);
                }
            }
        }
    </script>
</head>

<body>
    <div class="container">
        <form id="registrationForm" class="bd-blue" method="POST" action="regist_student.php"
            enctype="multipart/form-data">
            <div class="text-center bold-text mb-20">Form đăng ký sinh viên</div>
            <div id="errorMessages" class="error">
            </div>
            
            <div class="form-group">
                <div class="bg-green bd-blue p-10-20 w-30 me-20" for="name">Họ và
                    tên</div>
                <input class=" fl-1" type="text" id="name" name="name" required>
            </div>

            <div class="form-group">
                <div class="bg-green  bd-blue p-10-20 w-30 me-20" for="gender">
                    Giới tính</div>
                <div id="gender" name="gender" class="w-170 ">
                    <input type="radio" id="male" name="gender" value="Nam" required> Nam
                    <input type="radio" id="female" name="gender" value="Nữ" required> Nữ
                </div>
            </div>


            <div class="form-group">
                <div class="bg-green  bd-blue p-10-20 w-30  w-170 me-20" for="birthdate">Ngày sinh</div>
                <label for="birthdate" id="birthdate">Ngày Sinh:</label>
                <select name="nam_sinh" id="nam_sinh" required>
                    <?php
                    $currentYear = date("Y");
                    for ($year = $currentYear - 40; $year <= $currentYear - 15; $year++) {
                        echo '<option value="">' . $year . '</option>';
                    }
                    ?>
                </select>
                <select name="thang_sinh" id="thang_sinh" required>
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                        echo "<option value=''>$i</option>";
                    }
                    ?>
                </select>
                <select name="ngay_sinh" id="ngay_sinh" required>
                    <?php
                    for ($i = 1; $i <= 31; $i++) {
                        echo "<option value=''>$i</option>";
                    }
                    ?>
                </select><br><br>
            </div>

            <div class="form-group">
                <div class="bg-green  bd-blue p-10-20 w-30 me-20" for="address">Địa chỉ</div>
                <label for="address" id="address">Thành Phố:</label>
                <select name="thanh_pho" id="thanh_pho" onchange="updateDistricts();" required>
                    <option value=""></option>
                    <option value="Hà Nội">Hà Nội</option>
                    <option value="TP.HCM">TP.HCM</option>
                </select><br><br>

                <label for="quan">Quận:</label>
                <select name="quan" id="quan">
                    <option value=""></option>
                </select><br><br>

            </div>

            <div class="form-group">
                <div class="bg-green bd-blue p-10-20 w-30 " for="infor">Thông tin khác
                </div>
                <input class="" name="infor" id="infor"></input>
            </div>

            <div class="button-container" id="registerButton">
                <button type="submit">Đăng ký</button>
            </div>
        </form>
    </div>
</body>
<script>
    $(document).ready(function () {
        $("#registerButton").click(function () {
            var name = $("#name").val();
            var gender = $("input[name='gender']:checked").val();
            var department = $("#department").val();
            var birthdate = $("#birthdate").val();
            var address = $("#address").val();
            var flag = true;

            var errorMessages = [];

            if (name === "") {
                errorMessages.push("Hãy nhập tên.");
                flag = false
            }

            if (!gender) {
                errorMessages.push("Hãy chọn giới tính.");
                flag = false
            }

            if (birthdate === "") {
                errorMessages.push("Hãy chọn ngày sinh.");
                flag = false
            }

            if (address === "") {
                errorMessages.push("Hãy chọn địa chỉ.");
                flag = false
            }

            if (errorMessages.length > 0) {
                var errorMessageHtml = "";
                for (var i = 0; i < errorMessages.length; i++) {
                    errorMessageHtml += errorMessages[i] + "<br>";
                }
                $("#errorMessages").html(errorMessageHtml);
            } else {
                // Nếu không có lỗi, thực hiện đăng ký hoặc xử lý form ở đây
                $("#errorMessages").html(""); // Xóa thông báo lỗi cũ
            }
        });
    });
</script>

</html>