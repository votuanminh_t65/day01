<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style2.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</head>
<?php
$departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
?>
<?php
include "database.php";

$stmt = $conn->prepare("SELECT * FROM students");
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$result = $stmt->fetchAll();

?>

<body>
    <div class="container mt-4 w-50">

        <!-- <form action="">

        <table class="">
            <tr>
                <td>khoa</td>
                <td><input type="text"></td>
            </tr>
            <tr>
                <td>tu khoa</td>
                <td><input type="text"></td>
            </tr>

        </table>
        <input class="" type="submit" value="tim kiem">

        </div>

    </form> -->
   <div class="row">
   <form class="offset-2" action="" >
            <div class="mb-3 row">
                <label for="department" class="col-2 col-form-label">Khoa</label>
                <div class="col-4 ps-0">
                    <input type="text" class="form-control " id="department">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="inputKeyword" class="col-2 col-form-label">Từ khóa</label>
                <div class="col-4 ps-0">
                    <input type="text" class="form-control " id="inputKeyword">
                </div>
            </div>
    <div class="row mb-3 ">
        <button type="submit" class="btn btn-primary offset-2 col-2 find-btn">Tìm kiếm</button>
    </div>
        </form>
   </div>
        <div class="row">
            <p style="padding: 0 8px">
                Số sinh viên tìm thấy: xxx
            </p>
        </div>
        <div class="d-flex justify-content-end pe-5 ">
            <a href="register3.php">
                <button class="add-btn">Thêm</button>
            </a>
        </div>
        <div class="row">
            <table class="table">
                <tr>
                    <td>No</td>
                    <td>Tên sinh viên</td>
                    <td>Khoa</td>
                    <td  >
                        <label class="ms-4">action</label>
                        </td>
                </tr>
                <?php
                foreach ($result as $key => $value) {

                    ?>
                    <tr class="mb-3">
                        <td >
                            <?php echo $value['id'];
                            ?>
                        </td>
                        <td>
                            <?php echo $value['name'];
                            ?>
                        </td>
                        <td>
                            <?php 
                            echo $departments[$value['department']];
                            ?>
                        </td>
                        <td class="d-flex justify-content-center"><button class="me-2 db-btn">Xóa</button>
                        <button class="db-btn">Sửa</button></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>





</body>

</html>